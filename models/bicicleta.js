var bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;

}

bicicleta.prototype.toString = function(){

    return "id: "+ this.id + " | color: " + this.color + " | modelo: " + this.modelo + " | ubicacion: " + this.ubicacion; 

}

bicicleta.AllBicis = [];
bicicleta.add = function(aBici){

    bicicleta.AllBicis.push(aBici);

}

bicicleta.findById = function(id){

    var resultado = bicicleta.AllBicis.find(x => x.id == id);

    if(resultado)
        return resultado;
    else
        throw new Error(`No se encontro la bicicleta con id ${id}` );

}

bicicleta.remove = function (id){

    bicicleta.findById(id);

    for (var i = 0; i < bicicleta.AllBicis.length; i++) {
        if (bicicleta.AllBicis[i].id==id) {
            bicicleta.AllBicis.splice(i, 1);
            break;
            
        }
        
    }
}

var a = new bicicleta(1, "azul", "urbana", [-25.331681170485677, -57.55859295176418]);
var b = new bicicleta(2, "rojo", "urbana", [-25.33533696590266, -57.55487004562521]);
bicicleta.add(a);
bicicleta.add(b);

module.exports = bicicleta;