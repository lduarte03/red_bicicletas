var express = require('express');
var router = express.Router();
var bicicletaController = require("../controllers/bicicleta");
/* GET users listing. */
router.get('/', bicicletaController.bicicleta_list);

module.exports = router;
