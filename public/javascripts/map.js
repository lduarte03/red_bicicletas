var map = L.map('main_map', {
    center: [-25.336388, -57.562938],
    zoom: 13
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {foo: 'bar', attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicleta",
    success: function(result){

        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
            
        });

    }

});